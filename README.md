# go-skype

A Golang library to interface with Skype network.

This library is not based on any other library for Skype
(including Microsoft's). Instead, it is built via reverse-engineering the
[Skype Web](https://web.skype.com/en/) and aims to have the same
capabilities. **Because of that, things can break at any time after an update on
Skype servers**.

You will need to supply Skype login and password to use this library. Make sure
you are able to login with those credentials and that Skype Web **does not
display any additional prompts during that process** (like the prompt that asks
you to confirm your phone number).

**Check out [`example.go`](example.go) for simple usage instructions.** It
contains the annotated code for a simple Skype bot that replies to any message.
*Don't be intimidated by its size, most of the code there is for recovering from
internal Skype network failures.* More documentation is provided in the library
comments.

This package is `go get`table. The binary from the code inside `example.go` will
be available in your `$GOPATH/bin` after doing that.

Stable version is available on `master`, development happens on `dev`. Pull
requests are welcome. Make sure `go vet`, `go fmt` and
[`golint`](https://github.com/golang/lint) return nothing
(except for `/vendor`).

## Q&A

### Q:
I am able to login with my credentials, but I still get error when attempting
to use them with this lib.

### A:
This can happen if you use the library from a different host. Skype has
safeguards against what it deems to be _suspicious activity_ and logging in from
a different IP can trigger them.

To fix this, use Skype Web to login on the host where the library will be used.
In case you don't have a browser capable of that on the other host you will
need to proxy your browser's requests somehow (check out `ssh -D`
[manual](https://linux.die.net/man/1/ssh)).
