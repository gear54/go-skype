package skype

import (
	"errors"
	"time"
)

var zeroTime = time.Unix(0, 0)
var msnpQuery = "view=msnp24Equivalent"

// Resource represents a Resource in Skype network (like user or message).
// It contains fields that are required (but may not be sufficient) for
// bare representation of a Resource. This type is meant to be embedded
// into more specific types.
//
// Most embedding structures (like User or Message) have Refresh method that
// fetches the full Resource representation from Skype network.
type Resource struct {
	// Sess contains a pointer to the Skype Session that owns the Resource. Since all
	// Skype Sessions connect to the same network, this field only determines
	// credentials that will be used when performing operations on that Resource.
	Sess *Session

	// ID of the Resource is a string that uniquely identifies the resource. It
	// is necessary (but may not be sufficient) for constructing various URLs
	// associated with this resource in Skype network.
	//
	// This string will look different for different types of resources, but will
	// never include type IDs (like TypeIDUser).
	ID string
}

// Creates a new Resource after performing basic argument validation
func (s *Session) newResource(id string) (*Resource, error) {
	if s == nil {
		return nil, errors.New("skype/resource: session pointer cannot be nil")
	}

	if len(id) == 0 {
		return nil, errors.New("skype/resource: id cannot be empty")
	}

	return &Resource{s, id}, nil
}
