package skype

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"
)

// MemberRole represents the conversation member role.
type MemberRole uint8

const (
	// MemberRoleUser represents a User conversation member role.
	MemberRoleUser MemberRole = iota

	// MemberRoleAdmin represents an Admin conversation member role.
	MemberRoleAdmin
)

const (
	// TypeIDConversation is a prefix that conversation objects
	// have in IDs inside Skype network. A typical conversation
	// ID will look like this: 19:<hex-id>.
	TypeIDConversation = 19

	// APIPathConversations gives a logged in user's conversations URL
	// when joined with APIPathMe.
	APIPathConversations = "/conversations"

	// APIPathThreads gives a conversation URL when joined
	// with conversation ID.
	APIPathThreads = "/threads"

	// APIPathMessages gives a conversation's messages URL
	// when joined with APIPathMe, APIPathConversations and
	// conversation's network ID.
	APIPathMessages = "/messages"
)

// Conversation represents any chat the user is part of. It might be
// a personal 1:1 conversation (see IsPersonal) or a group chat.
//
// Note that Skype network also has a thread entity. Conversation struct
// contains data from both entities. Every group (but not personal)
// conversation has a matching thread. In Skype network, conversation
// is a thread from the logged-in user's perspective.
//
// ID of this resource is a 32 character long hexadecimal string for
// group chats and the other user ID for personal chats.
type Conversation struct {
	Resource

	// IsPersonal indicates that this is a personal conversation
	// between two users.
	IsPersonal bool

	// Topic is a conversation topic. It is an empty string for
	// personal conversations.
	//
	// It appears that this is not always accurate for conversations
	// identified from new message event. To get the correct value
	// use RefreshThread.
	Topic string

	// Members is an array with conversation members. It is an array
	// containing all users in a group chat (including the one that
	// the Session uses) or a single element for the other user in
	// a personal conversation.
	Members map[*User]MemberRole
}

// Copy performs a deep copy of the Conversation. The new Conversation
// and all Skype Resources in its properties will refer to the same
// entities on the Skype network.
func (c *Conversation) Copy() *Conversation {
	convCopy := &*c
	membersCopy := map[*User]MemberRole{}

	for user, role := range convCopy.Members {
		membersCopy[user] = role
	}

	convCopy.Members = membersCopy

	return convCopy
}

// NetworkID returns a string that is used when specifying this resource
// in requests to Skype network. For Conversation, it looks like: 19:<hex-id>.
// For personal conversations, it is the same as the other user's NetworkID.
func (c *Conversation) NetworkID() string {
	if c.IsPersonal {
		return fmt.Sprintf("%d:%s", TypeIDUser, c.ID)
	}

	return fmt.Sprintf("%d:%s@thread.skype", TypeIDConversation, c.ID)
}

// NewConversation creates a new Conversation for a given conversation ID (group)
// or user username (personal). You can use this function to later call Refresh on the
// resulting struct to retrieve information about arbitrary conversations.
func (s *Session) NewConversation(id string, isPersonal bool) (*Conversation, error) {
	res, err := s.newResource(id)
	if err != nil {
		return nil, err
	}

	conv := &Conversation{Resource: *res, IsPersonal: isPersonal, Members: map[*User]MemberRole{}}

	if isPersonal {
		user, err := s.NewUser(id)
		if err != nil {
			return nil, fmt.Errorf(
				"skype/conversation: failed to create parsonal conversation member: %s",
				err.Error(),
			)
		}

		conv.Members[user] = MemberRoleUser
	}

	return conv, nil
}

var (
	rxConversationIDFromNetworkID = regexp.MustCompile(fmt.Sprintf(
		"(?:%d:([^@]+)@thread.skype|%s)",
		TypeIDConversation,
		rxUserIDFromNetworkID.String(),
	))

	rxConversationFromURL = regexp.MustCompile(fmt.Sprintf(
		"%s/%s",
		APIPathMe+APIPathConversations,
		rxConversationIDFromNetworkID.String(),
	))
)

// ConversationFromNetworkID constructs a Conversation from network ID string that
// looks like: 19:<conversation-id>@thread.skype (group) or 8:<username> (personal).
func (s *Session) ConversationFromNetworkID(networkID string) (*Conversation, error) {
	match := rxConversationIDFromNetworkID.FindStringSubmatch(networkID)
	if match == nil {
		return nil, fmt.Errorf("skype/conversation: failed to extract conversation ID from '%s'", networkID)
	}

	if len(match[1]) > 0 {
		return s.NewConversation(match[1], false)
	}

	return s.NewConversation(match[2], true)
}

// ConversationFromURL constructs a Conversation struct from the logged in
// user's conversations URL.
func (s *Session) ConversationFromURL(resURL string) (*Conversation, error) {
	parsedURL, err := url.Parse(resURL)
	if err != nil {
		return nil, err
	}

	match := rxConversationFromURL.FindStringSubmatch(parsedURL.Path)
	if match == nil {
		return nil, fmt.Errorf("skype/conversation: failed to extract conversation ID from '%s'", resURL)
	}

	if len(match[1]) > 0 {
		return s.NewConversation(match[1], false)
	}

	return s.NewConversation(match[2], true)
}

type conversationResource struct {
	Properties       struct{}
	ThreadProperties struct{}
}

func (s *Session) conversationFromResource(resourceJSON json.RawMessage) (*Conversation, error) {
	//resource := conversationResource{}

	return nil, nil
}

type threadResource struct {
	ID         string `json:"id"`
	Properties struct {
		Creator     string `json:"creator"`
		Topic       string `json:"topic"`
		BannedUsers string `json:"banneduserlist"`
	}
	Members []struct {
		ID   string `json:"id"`
		Role string `json:"role"`
	}
}

func (s *Session) conversationFromThreadResource(resourceJSON json.RawMessage) (*Conversation, error) {
	resource := threadResource{}
	err := json.Unmarshal(resourceJSON, &resource)
	if err != nil {
		return nil, fmt.Errorf(
			"skype/conversation: failed to parse thread resource: %s",
			err.Error(),
		)
	}

	conv, err := s.ConversationFromNetworkID(resource.ID)
	if err != nil {
		return nil, err
	}

	conv.Topic = resource.Properties.Topic
	conv.Members = map[*User]MemberRole{}

	for _, member := range resource.Members {
		user, err := s.UserFromNetworkID(member.ID)
		if err != nil {
			return nil, fmt.Errorf(
				"skype/conversation: failed to create member: %s",
				err.Error(),
			)
		}

		var role MemberRole

		switch member.Role {
		case "User":
			role = MemberRoleUser
		case "Admin":
			role = MemberRoleAdmin
		default:
			return nil, fmt.Errorf(
				"skype/conversation: unknown role for member '%s': %s",
				user.ID,
				member.Role,
			)
		}

		conv.Members[user] = role
	}

	return conv, nil
}

// Refresh the information about this conversation. This will
// attempt to fill all the fields it can using the information
// from Skype network.
func (c *Conversation) Refresh() error {
	return nil
}

// RefreshThread refreshes the information about the underlying thread.
// Updated properties: Topic, Members.
//
// This operation only makes sense for non-personal (see IsPersonal)
// conversations, it will return error if used on a personal conversation.
func (c *Conversation) RefreshThread() error {
	if c.IsPersonal {
		return errors.New("skype/conversation: personal conversations don't have associated threads")
	}

	s := c.Sess

	req, _ := http.NewRequest(
		http.MethodGet,
		fmt.Sprintf(
			"%s/%s?%s",
			s.APIURLRoot()+APIPathThreads,
			c.NetworkID(),
			msnpQuery,
		),
		nil,
	)

	resp, err := s.Do(req)
	body, err := expectCodeGetBody(http.StatusOK, resp, err)
	if err != nil {
		return fmt.Errorf("skype/conversation: failed to refresh thread: %s", err.Error())
	}

	conv, err := s.conversationFromThreadResource(json.RawMessage(body))
	if err != nil {
		return err
	}

	c.Topic = conv.Topic
	c.Members = conv.Members

	return nil
}

type sendMessageResponse struct {
	OriginalArrivalTime int64 `json:"OriginalArrivalTime"`
}

// SendMessage constructs a message out of MessageParts and sends it to the
// conversation. It returns OriginalArrvialTime as reported by the Skype network.
func (c *Conversation) SendMessage(parts ...MessagePart) (time.Time, error) {
	s := c.Sess

	resp, err := s.Post(
		fmt.Sprintf(
			"%s/%s",
			s.APIURLRoot()+APIPathMe+APIPathConversations,
			c.NetworkID()+APIPathMessages,
		),
		"application/json",
		strings.NewReader(outgoingMessageJSON(parts...)),
	)
	body, err := expectCodeGetBody(http.StatusCreated, resp, err)
	if err != nil {
		return zeroTime, fmt.Errorf("skype/conversation: %s", err.Error())
	}

	respJSON := sendMessageResponse{}
	err = json.Unmarshal([]byte(body), &respJSON)
	if err != nil {
		return zeroTime, fmt.Errorf(
			"skype/conversation: failed to parse sent message resource: %s",
			err.Error(),
		)
	}

	return time.Unix(0, respJSON.OriginalArrivalTime*1e6), nil
}
