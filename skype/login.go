package skype

import (
	"errors"
	"fmt"
	"golang.org/x/net/publicsuffix"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"regexp"
	"strings"
	"time"
)

const (
	apiURLTemplate = "https://%s/v1"

	// LoginURL is the frst URL used in the login process.
	LoginURL = "https://login.skype.com/login"

	// LogoutURL is the frst URL used in the logout process.
	LogoutURL = "https://login.skype.com/logout"
)

var (
	rxPPSecureURL       = regexp.MustCompile(`urlPost:'([^']+)'`)
	rxRegistrationToken = regexp.MustCompile(`registrationToken=([^;]+); expires=([^;]+); endpointId={([^}]+)}?`)

	// We are reduced to parsing HTML with regex cuz available parsers are primitive.
	rxHTMLInput = regexp.MustCompile("<input ([^>]+)/?>")

	rxTmplHTMLAttr      = `%s="([^"]+)"`
	rxTmplHTMLFormID    = `<form ([^>]*id="%s"[^>]*)>(.*)</form>`
	rxTmplHTMLInputName = `<input ([^>]*name="%s"[^>]*)\/?>`
)

func htmlAttrValue(attrs, attrName string) (string, error) {
	match := regexp.MustCompile(
		fmt.Sprintf(rxTmplHTMLAttr, attrName),
	).FindStringSubmatch(attrs)
	if match == nil {
		return "", fmt.Errorf("no such attribute: %s", attrName)
	}

	return match[1], nil
}

func htmlInputValueByName(body, name string) (string, error) {
	match := regexp.MustCompile(fmt.Sprintf(
		rxTmplHTMLInputName,
		name,
	)).FindStringSubmatch(body)
	if match == nil {
		return "", fmt.Errorf("failed to find input with name '%s'", name)
	}

	value, err := htmlAttrValue(match[1], "value")
	if err != nil {
		return "", fmt.Errorf("failed to find value for input '%s': %s", name, err.Error())
	}

	return value, nil
}

func htmlFormRedirect(body, formID string) (*http.Request, error) {
	match := regexp.MustCompile(
		fmt.Sprintf(rxTmplHTMLFormID, formID),
	).FindStringSubmatch(body)
	if match == nil {
		return nil, fmt.Errorf("failed to find form with ID: %s", formID)
	}

	method, err := htmlAttrValue(match[1], "method")
	if err != nil {
		return nil, errors.New("failed to find form method attribute")
	}

	method = strings.ToUpper(method)

	reqURL, err := htmlAttrValue(match[1], "action")
	if err != nil {
		return nil, errors.New("failed to find form action attribute")
	}

	inputs := rxHTMLInput.FindAllStringSubmatch(match[2], -1)
	reqBody := url.Values{}

	for _, input := range inputs {
		key, err := htmlAttrValue(input[1], "name")
		if err != nil {
			continue
		}

		value, err := htmlAttrValue(input[1], "value")
		if err != nil {
			continue
		}

		reqBody.Set(key, value)
	}

	req, err := http.NewRequest(method, reqURL, strings.NewReader(reqBody.Encode()))
	if err != nil {
		return nil, err
	}

	if req.Method == http.MethodPost {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}

	return req, nil
}

func (s *Session) refreshSkypeToken(resp *http.Response, err error) error {
	body, err := expectCodeGetBody(http.StatusOK, resp, err)
	if err != nil {
		return fmt.Errorf(
			"failed to complete Skype token request: %s",
			err.Error(),
		)
	}

	for _, formID := range []string{"fmHF", "redirectForm"} {
		req, err := htmlFormRedirect(body, formID)
		if err != nil {
			return fmt.Errorf(
				"failed to parse '%s' redirect data: %s",
				formID,
				err.Error(),
			)
		}

		resp, err = s.Do(req)
		body, err = expectCodeGetBody(http.StatusOK, resp, err)
		if err != nil {
			return fmt.Errorf(
				"failed to follow '%s' redirect: %s",
				formID,
				err.Error(),
			)
		}
	}

	skypeID, err := htmlInputValueByName(body, "skypeid")
	if err != nil {
		return fmt.Errorf("failed to find Skype ID: %s", err.Error())
	}

	s.me, err = s.NewUser(skypeID)
	if err != nil {
		return fmt.Errorf("failed to create 'Me' user: %s", err.Error())
	}

	skypeToken, err := htmlInputValueByName(body, "skypetoken")
	if err != nil {
		return fmt.Errorf("failed to find Skype token value: %s", err.Error())
	}

	s.mu.Lock()
	s.loginData.SkypeToken = skypeToken
	s.mu.Unlock()

	return nil
}

func loginErrorF(format string, a ...interface{}) error {
	return fmt.Errorf(fmt.Sprintf("skype/login: %s", format), a...)
}

// DefaultTransport returns a pointer to http.Transport that Login uses by default.
func DefaultTransport() *http.Transport {
	return &http.Transport{
		TLSHandshakeTimeout: 5 * time.Second,
		MaxIdleConns:        10,
		IdleConnTimeout:     5 * time.Second,
	}
}

// Login to Skype network using login and password. You can supply your own
// *http.Transport as the last argument to adjust various HTTP parameters for the
// resulting Session. Returns Skype Session you can use to perform actions as
// the given account.
//
// The returned session does not store your supplied credentials but it will
// contain HTTP cookies and other tokens Skype network responds with during
// login process.
func Login(login, password string, transport *http.Transport) (*Session, error) {
	s := &Session{apiURL: fmt.Sprintf(apiURLTemplate, APIHost)}

	if transport == nil {
		transport = DefaultTransport()
	}

	cookieJar, _ := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})

	s.client = &http.Client{
		Transport: transport,
		Jar:       cookieJar,
		Timeout:   0,
	}

	req, _ := http.NewRequest(http.MethodGet, LoginURL, nil)

	resp, err := s.Do(req)
	body, err := expectCodeGetBody(http.StatusOK, resp, err)
	if err != nil {
		return nil, loginErrorF("failed to complete login URL request: %s", err.Error())
	}

	ppft, err := htmlInputValueByName(body, "PPFT")
	if err != nil {
		return nil, loginErrorF("failed to find PPFT value: %s", err.Error())
	}

	match := rxPPSecureURL.FindStringSubmatch(body)
	if match == nil {
		return nil, loginErrorF("failed to find 'urlPost' JS key")
	}

	resp, err = s.PostForm(
		match[1],
		url.Values{
			"login":  []string{login},
			"passwd": []string{password},
			"PPFT":   []string{ppft},
		},
	)
	err = s.refreshSkypeToken(resp, err)
	if err != nil {
		return nil, loginErrorF(
			"failed to get Skype token (usually means credentials are incorrect): %s",
			err.Error(),
		)
	}

	const endpointErrorPrefix = "failed to create endpoint:"

	req, _ = http.NewRequest(
		http.MethodPost,
		s.APIURLRoot()+APIPathMe+APIPathEndpoints,
		strings.NewReader(endpointCreateBody),
	)

	s.client.CheckRedirect = func(_ *http.Request, _ []*http.Request) error { return http.ErrUseLastResponse }
	resp, err = s.do(req, s.loginData.SkypeToken, "")
	s.client.CheckRedirect = nil
	if err != nil {
		return nil, loginErrorF("%s HTTP error: %s", endpointErrorPrefix, err.Error())
	}
	resp.Body.Close()

	if !((resp.StatusCode == http.StatusCreated) || (resp.StatusCode == http.StatusMovedPermanently)) {
		return nil, loginErrorF(
			"%s received HTTP %s (expected %s or %s)",
			endpointErrorPrefix,
			resp.Status,
			httpStatus(http.StatusCreated),
			httpStatus(http.StatusMovedPermanently),
		)
	}

	err = s.setRegToken(resp.Header)
	if err != nil {
		return nil, loginErrorF("%s %s", endpointErrorPrefix, err.Error())
	}

	if resp.StatusCode == http.StatusMovedPermanently {
		newAPIURL, err := url.Parse(resp.Header.Get("Location"))
		if err != nil {
			return nil, loginErrorF(
				"%s: HTTP %s received but Location header couldn't be parsed: %s",
				endpointErrorPrefix,
				resp.Status,
				err.Error(),
			)
		}

		s.apiURL = fmt.Sprintf(apiURLTemplate, newAPIURL.Host)
		err = s.ensureEndpoint(false, true)
	} else {
		err = s.ensureEndpoint(false, false)
	}

	if err != nil {
		return nil, loginErrorF(
			"failed to ensure endpoint readiness: %s",
			err.Error(),
		)
	}

	return s, nil
}

// Logout removes the created endpoint and logs out from Skype network.
func (s *Session) Logout() error {
	req, _ := http.NewRequest(
		http.MethodDelete,
		s.APIURLRoot()+APIPathMe+APIPathEndpoints+APIPathSelf,
		nil,
	)

	s.Do(req) // attempt to clean up but ignore errors

	s.mu.Lock()
	s.loginData = LoginData{}
	s.mu.Unlock()

	jar := s.client.Jar
	logoutURL, _ := url.Parse(LogoutURL)
	var toRemove []*http.Cookie

	for _, cookie := range jar.Cookies(logoutURL) {
		toRemove = append(toRemove, &http.Cookie{
			Name:  cookie.Name,
			Value: "",
		})
	}

	s.client.Jar.SetCookies(logoutURL, toRemove)
	req, _ = http.NewRequest(http.MethodGet, LogoutURL, nil)

	resp, err := s.Do(req)
	_, err = expectCodeGetBody(http.StatusOK, resp, err)
	if err != nil {
		return sessionErrorF("failed to complete cookie unset request: %s", err.Error())
	}

	return err
}
