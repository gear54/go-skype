package skype

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

// UserPresenceHandler is a function that handles user presence events.
type UserPresenceHandler func(*UserPresence, EventMeta)

// OnUserPresence attaches a user presence handler to the session.
func (s *Session) OnUserPresence(handler UserPresenceHandler) {
	s.mu.Lock()
	s.onUserPresence = handler
	s.mu.Unlock()
}

// MessageHandler is a function that handles message events.
type MessageHandler func(*Message, EventMeta)

// OnNewMessage attaches a message event handler to the session.
//
// Note that your own messages and actions will also trigger this
// event so if you are replying to each new message caught here, an
// infinite loop will be triggered. To avoid it, check the Sender of
// the message against the Session's Me field.
func (s *Session) OnNewMessage(handler MessageHandler) {
	s.mu.Lock()
	s.onMessage = handler
	s.mu.Unlock()
}

// ThreadUpdateHandler is a function that handles thread update events.
type ThreadUpdateHandler func(*Conversation, EventMeta)

// OnThreadUpdate attaches a thread update event handler to the session.
// Thread updates will be triggered, for example, when a member leaves
// a conversation.
func (s *Session) OnThreadUpdate(handler ThreadUpdateHandler) {
	s.mu.Lock()
	s.onThreadUpdate = handler
	s.mu.Unlock()
}

// PollError is an interface for representing an error encountered while
// polling Skype network.
type PollError interface {
	error

	// Response returns HTTP response that was received during polling. Note
	// that its body will already be closed and content will be available via
	// separate Body function.
	//
	// If we couldn't complete or even start the HTTP poll request,
	// this is an nil pointer.
	Response() *http.Response

	// Body returns poll response body.
	//
	// If we couldn't complete or even start the HTTP poll request,
	// this is an empty string.
	Body() string

	// Chunk returns part of HTTP response body that caused the error.
	// It doesn't necessarily mean parsing error.
	//
	// If we couldn't complete or even start the HTTP poll request,
	// this is an empty string.
	//
	// If the wrong HTTP response code was received or the response body
	// couldn't be parsed, it's the same as Body. If some individual
	// event message couldn't be parsed or had some other error, it is the
	// respective event message.
	Chunk() string

	// PanicValue returns a value of a panic that occurred during event handler
	// execution. If there was a parsing error (i.e. we didn't get to the stage
	// of running event handlers), this is a nil interface.
	PanicValue() interface{}
}

type pollError struct {
	msg,
	chunk,
	body string
	resp       *http.Response
	panicValue interface{}
}

func (pe *pollError) Error() string            { return pe.msg }
func (pe *pollError) Response() *http.Response { return pe.resp }
func (pe *pollError) Body() string             { return pe.body }
func (pe *pollError) Chunk() string            { return pe.chunk }
func (pe *pollError) PanicValue() interface{}  { return pe.panicValue }

func (s *Session) handleUserPresence(resource json.RawMessage, meta EventMeta) PollError {

	return nil
}

func (s *Session) handleNewMessage(resourceJSON json.RawMessage, meta EventMeta) (result PollError) {
	message, err := s.messageFromResource(resourceJSON)

	pollErr := &pollError{chunk: string(resourceJSON)}

	if err != nil {
		pollErr.msg = err.Error()

		return pollErr
	}

	s.mu.RLock()
	handler := s.onMessage
	s.mu.RUnlock()

	if handler != nil {
		defer func() {
			if panicValue := recover(); panicValue != nil {
				pollErr.msg = sessionErrorF("OnNewMessage handler panic", err.Error()).Error()
				pollErr.panicValue = panicValue
				result = pollErr
			}
		}()

		handler(message, meta)
	}

	return nil
}

func (s *Session) handleThreadUpdate(resourceJSON json.RawMessage, meta EventMeta) (result PollError) {
	conv, err := s.conversationFromThreadResource(resourceJSON)

	pollErr := &pollError{chunk: string(resourceJSON)}

	if err != nil {
		pollErr.msg = err.Error()

		return pollErr
	}

	s.mu.RLock()
	handler := s.onThreadUpdate
	s.mu.RUnlock()

	if handler != nil {
		defer func() {
			if panicValue := recover(); panicValue != nil {
				pollErr.msg = sessionErrorF("OnThreadUpdate handler panic", err.Error()).Error()
				pollErr.panicValue = panicValue
				result = pollErr
			}
		}()

		handler(conv, meta)
	}

	return nil
}

// EventMeta contains fields sent by Skype network with any event.
type EventMeta struct {
	// Time is a timestamp that is associated with this event.
	Time time.Time

	// ResourceURL is a URL that can be used to access the resource in
	// this event.
	ResourceURL *url.URL
}

type eventMessage struct {
	ResourceType string          `json:"resourceType"`
	Time         time.Time       `json:"time"`
	ResourceURL  string          `json:"resourceLink"`
	Resource     json.RawMessage `json:"resource"`
}

type pollResponse struct {
	EventMessages []json.RawMessage `json:"eventMessages"`
}

func (s *Session) handlePollBody(body []byte) PollError {
	resp := pollResponse{}
	err := json.Unmarshal(body, &resp)

	pollErr := &pollError{}

	if err != nil {
		pollErr.msg = sessionErrorF(
			"failed to parse the response: %s",
			err.Error(),
		).Error()

		pollErr.chunk = string(body)

		return pollErr
	}

	for _, eventJSON := range resp.EventMessages {
		pollErr.chunk = string(eventJSON)
		event := eventMessage{}
		err := json.Unmarshal(eventJSON, &event)

		if err != nil {
			pollErr.msg = sessionErrorF(
				"failed to parse the event message: %s",
				err.Error(),
			).Error()

			return pollErr
		}

		resourceURL, err := url.Parse(event.ResourceURL)

		if err != nil {
			pollErr.msg = sessionErrorF(
				"skype/session: failed to parse 'resourceLink': %s",
				err.Error(),
			).Error()

			return pollErr
		}

		var resourceHandleErr PollError
		eventMeta := EventMeta{event.Time, resourceURL}

		switch event.ResourceType {
		case "EndpointPresence":
		case "ConversationUpdate":
		case "UserPresence":
			resourceHandleErr = s.handleUserPresence(event.Resource, eventMeta)
		case "NewMessage":
			resourceHandleErr = s.handleNewMessage(event.Resource, eventMeta)
		case "ThreadUpdate":
			resourceHandleErr = s.handleThreadUpdate(event.Resource, eventMeta)
		default:
			pollErr.msg = sessionErrorF(
				"unknown 'resourceType': '%s'",
				event.ResourceType,
			).Error()

			return pollErr
		}

		if resourceHandleErr != nil {
			pollErr.msg = resourceHandleErr.Error()
			pollErr.chunk = resourceHandleErr.Chunk()
			pollErr.panicValue = resourceHandleErr.PanicValue()

			return pollErr
		}
	}

	return nil
}

func (s *Session) poll() PollError {
	ctx, cancel := context.WithCancel(context.Background())

	s.mu.Lock()
	s.cancelPolling = cancel
	s.mu.Unlock()

	for {
		req, _ := http.NewRequest(
			http.MethodPost,
			s.APIURLRoot()+APIPathMe+APIPathEndpoints+APIPathSelf+
				APIPathSubscriptions+APIPathPoll,
			nil,
		)

		resp, err := s.Do(req.WithContext(ctx))

		select {
		case <-ctx.Done():
			if err == nil {
				resp.Body.Close()
			}

			return nil
		default:
		}

		pollErr := &pollError{resp: resp}

		if err != nil {
			pollErr.msg = sessionErrorF(
				"HTTP error: %s",
				err.Error(),
			).Error()

			return pollErr
		}

		body, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()

		if err == nil {
			pollErr.body = string(body)
			pollErr.chunk = string(body)
		}

		if resp.StatusCode != http.StatusOK {
			pollErr.msg = sessionErrorF(
				"received HTTP %s (expected %s)",
				resp.Status,
				httpStatus(http.StatusOK),
			).Error()

			return pollErr
		}

		if err != nil {
			pollErr.msg = sessionErrorF(
				"failed to read body: %s",
				err.Error(),
			).Error()

			return pollErr
		}

		bodyHandleError := s.handlePollBody([]byte(body))
		if bodyHandleError != nil {
			pollErr.msg = bodyHandleError.Error()
			pollErr.chunk = bodyHandleError.Chunk()
			pollErr.panicValue = bodyHandleError.PanicValue()

			return pollErr
		}
	}
}

// IsPolling returns true if polling is in progress for a given session.
func (s *Session) IsPolling() bool {
	s.mu.RLock()
	result := s.cancelPolling != nil
	s.mu.RUnlock()

	return result
}

// StopPolling stops the ongoing polling for a session. If the session
// is not polling, it simply returns instantly.
func (s *Session) StopPolling() {
	s.mu.Lock()
	if s.cancelPolling != nil {
		s.cancelPolling()
		s.cancelPolling = nil
	}
	s.mu.Unlock()
}

// StartPolling starts polling the Skype network for events. If you
// don't call this function, none of your event handlers will
// ever be called.
//
// This function blocks until polling is stopped. Polling is stopped
// on any error that occurs (like HTTP failure or parsing failure).
// Polling is also stopped if any event handler panics. The panic value
// is then returned as part of PollError.
//
// Attempting to poll while already polling is also an error that is
// returned immediately. This error does not cancel polling. To check
// if the session polling is already underway, use IsPolling.
func (s *Session) StartPolling() PollError {
	if s.IsPolling() {
		return &pollError{
			msg: sessionErrorF("already polling").Error(),
		}
	}

	err := s.poll()
	if err != nil {
		s.StopPolling()
	}

	return err
}
