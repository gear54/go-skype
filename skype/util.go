package skype

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

// Check for errors and return body without cruft.
func expectCodeGetBody(expectedCode int, resp *http.Response, err error) (string, error) {
	if err != nil {
		return "", fmt.Errorf("HTTP error: %s", err.Error())
	}

	if resp.StatusCode != expectedCode {
		return "", fmt.Errorf(
			"received HTTP %s (expected %s)",
			resp.Status,
			httpStatus(expectedCode),
		)
	}

	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()

	if err != nil {
		return "", fmt.Errorf("failed to read body: %s", err.Error())
	}

	return string(body), nil
}

// Get status (like http.Response.Status) for an arbitrary HTTP code.
func httpStatus(code int) string {
	return fmt.Sprintf(
		"%d %s",
		code,
		http.StatusText(code),
	)
}
