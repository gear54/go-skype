package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/gear54/go-skype/skype"
)

const (
	// Same time format as used by log package by default.
	timeFormat = "2006/01/02 15:04:05"

	// Desired Skype status when the bot is working.
	status = skype.StatusOnline
)

func main() {
	tr := skype.DefaultTransport()

	// Optional debugging mode (for use with something like Fiddler or mitmproxy).
	if len(os.Getenv("DEBUG")) > 0 {
		// Accept self-signed TLS certificates.
		tr.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

		// Get proxy from HTTP_PROXY environment variable.
		tr.Proxy = http.ProxyFromEnvironment
	}

	// Login to Skype network using credentials from the environment
	session, err := skype.Login(
		os.Getenv("SKYPE_USER"),
		os.Getenv("SKYPE_PASSWORD"),
		tr,
	)

	// And panic on any error
	if err != nil {
		panic(err)
	}

	loginData := session.LoginData()

	log.Printf(
		"Logged in, endpoint ID: %s, registration token expires at: %s\n",
		loginData.EndpointID,
		loginData.RegTokenExpires.Format(timeFormat),
	)

	// Set our status after logging in.
	session.SetStatus(status)

	// Subscribe to incoming message events.
	session.OnNewMessage(func(message *skype.Message, a skype.EventMeta) {
		// Ignore our own messages.
		if message.Sender.ID == session.Me().ID {
			return
		}

		// Ignore group chats.
		if !message.Conv.IsPersonal {
			return
		}

		// Log content of messages and reply to them. React only to new messages,
		// ignore edits and removals.
		if message.Type == skype.MessageNew {
			// Log the incoming message properties and raw content.
			log.Printf(
				"New message: (%s) %s: %s\n",
				message.Conv.Topic,
				message.Sender.DisplayName,
				message.RawContent,
			)

			// Start constructing a reply.
			var reply []skype.MessagePart

			// Add the quote of incoming message at the start.
			reply = append(reply, message.Quote())

			// Count emoticons and message length.
			emoticons := 0
			characters := 0

			for _, part := range message.Content {
				if _, ok := part.(skype.MsgEmoticon); ok {
					emoticons++
				}

				characters += len([]rune(part.Text()))
			}

			// Add the greeting to the personalized reply.
			reply = append(
				reply,
				skype.MsgText(fmt.Sprintf(
					"Hey there, %s! Your message has %d emoticon(s) and is %d character(s) long.",
					message.Sender.DisplayName,
					emoticons,
					characters,
				)),
			)

			// Send our reply.
			_, err := message.Reply(reply...)

			// And log any errors that might've occurred.
			if err != nil {
				log.Printf(
					"Error while replying: %s",
					err.Error(),
				)
			}
		}
	})

	// The code from here to the end of the file ensures ability to recover from Skype
	// network errors that are going to creep up after prolonged operation. It is not
	// recommended to omit or change logic of this code in your own bots unless you
	// really know what you are doing :).

	// Refresh our registration token over N (N=5) tries spread evenly over previous
	// token TTL minus one minute. Attempts are spread like that to ensure any temporary
	// failures disappear until the next attempt. A minute is subtracted so that we have
	// some time on the last attempt before the token expires, this guarantees polling
	// without interruption.
	go func() {
		tryCount := 5
		expires := session.LoginData().RegTokenExpires

	nextUpdate:
		interval := expires.Add(-1*time.Minute).Sub(time.Now()) /
			time.Duration(tryCount)

		for i := 1; i <= tryCount; i++ {
			left := tryCount - i

			log.Printf(
				"Next registration token refresh at: %s\n",
				time.Now().Add(interval).Format(timeFormat),
			)

			time.Sleep(interval)

			log.Println("Refreshing registration token and ensuring endpoint availability...")

			// Re-authenticate and ensure endpoint availability, this gives us fresh registration
			// token with the new expiration date.
			err := session.EnsureEndpoint(true)
			if err != nil {
				if left > 0 {
					log.Printf(
						"Failed to ensure endpoint availability (%d tries left): %s\n",
						left,
						err.Error(),
					)
				} else {
					log.Printf(
						"Failed to ensure endpoint availability over %d tries, exiting\n",
						tryCount,
					)

					os.Exit(1)
				}

				continue
			}

			expires = session.LoginData().RegTokenExpires

			log.Printf(
				"Endpoint availability ensured successfully, registration token expires at: %s\n",
				expires.Format(timeFormat),
			)

			goto nextUpdate
		}
	}()

	// Handle SIGINT (^C) gracefully.
	interrupted := make(chan os.Signal, 1)
	signal.Notify(interrupted, os.Interrupt)

	go func() {
		<-interrupted
		log.Println("SIGINT received, logging out...")

		// Logout and exit.
		session.Logout()
		os.Exit(1)
	}()

	go func() {
		log.Println("Sending heartbeat...")

		for {
			time.Sleep(30 * time.Second)

			err := session.Activate(time.Minute)
			if err != nil {
				log.Printf(
					"Heartbeat error: %s\n",
					err.Error(),
				)
			}
		}
	}()

	for {
		log.Println("Polling...")

		// Start polling for events and block.
		pollErr := session.StartPolling()
		resp := pollErr.Response()

		// If an error occurs, log it as well as the chunk of the response that caused it.
		log.Printf("Poll error: %s (chunk: %s, panic: %s)\n",
			pollErr.Error(),
			pollErr.Chunk(),
			pollErr.PanicValue(),
		)

		// If the Skype network returns non-200 HTTP code, it might mean something is wrong
		// with our endpoint. So we try to ensure its availability (without re-authentication)
		// over N (N=27) tries.
		if (resp != nil) && (resp.StatusCode != http.StatusOK) {
			log.Printf(
				"HTTP %s received during polling, response body:\n%s\n",
				resp.Status,
				pollErr.Body(),
			)

			tryCount := 27 // 27 attempts over the course of 6+ hours, intervals increase each time

			for i := 1; i <= tryCount; i++ {
				left := tryCount - i

				log.Println("Ensuring endpoint availability...")

				err := session.EnsureEndpoint(false)
				if err != nil {
					if left > 0 {
						log.Printf(
							"Failed to ensure endpoint availability (%d tries left): %s\n",
							left,
							err.Error(),
						)
					} else {
						log.Printf(
							"Failed to ensure endpoint availability over %d tries, exiting\n",
							tryCount,
						)

						// Logout and exit.
						session.Logout()

						return
					}

					nextTryIn := time.Duration(i) * time.Minute

					log.Printf(
						"Next attempt to ensure endpoint availability at: %s\n",
						time.Now().Add(nextTryIn).Format(timeFormat),
					)

					// Next try after 1 minute, then after 2, then 3 and up to N.
					time.Sleep(nextTryIn)

					continue
				}

				log.Println("Endpoint availability ensured successfully!")

				// Need to set status again since previous endpoint was probably lost.
				session.SetStatus(status)

				break
			}
		}
	}
}
